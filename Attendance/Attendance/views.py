from django.shortcuts import render, redirect #render class is used when we want to render html
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm #it will design the sign up form
from  django.contrib.auth.models import User 
from django.contrib.auth import authenticate,login, logout
from django.db import IntegrityError
from  Attendance.models import attendance,record
from django.contrib import messages
from Attendance.forms import  stforms



def signnewuser(request):
	if request.method=="POST":#posting the data such like username and password
		if request.POST.get('password1')==request.POST.get('password2'):#condition1
			try:# for secondition
                #saveuser is variable
				saveuser=User.objects.create_user(request.POST.get('username'),password=request.POST.get('password1'))
				saveuser.save() #if pwd1 and pwd2 same
				return render(request,'Signup.html',{'form':UserCreationForm(),'info':'The User'+""+request.POST.get('username')+' is successfully..!'})#id same mean it will save 
			except IntegrityError:

				return render(request,'Signup.html',{'form':UserCreationForm(),'error':'The User'+request.POST.get('username')+'is Already Exist...!'})

		else:

			return render(request,'Signup.html',{'form':UserCreationForm(),'error':'The Passwords Are Not Matching..!'})
			
	else:

		return render(request,'Signup.html',{'form':UserCreationForm})



def loginuser(request):
	if request.method=="POST":
		#loginsuccess is varible
		loginsuccess=authenticate(request,username=request.POST.get('username'),password=request.POST.get('password'))# here requesting and posting the username and password
		if loginsuccess is None: #condition
			return render(request,'login.html',{'form':AuthenticationForm(),'error':'The Username and Passwords Are wrong..!'})
		else:
			login(request,loginsuccess)
			return redirect('home')#if username and password is correct it will take toward homepage
	else:
		return render(request,'login.html',{'form':AuthenticationForm()})


def home(request):
	return render(request,'home.html')

def logoutpage(request):
	if request.method=="POST":
		logout(request)
		return redirect('loginuser')

def  studentinfo(request):
	#variable
	stud=attendance.objects.all()#it will return query set
	return render(request, 'atten.html',{"stu":stud})#dict


def stdisplay(request):
	results=attendance.objects.all()
	return render(request,'atten.html',{"attendance": results})
	s


def stinsert(request):
	if request.method=="POST":#condition
		#value that we gonna post
		if request.POST.get('name') and request.POST.get('phonenumber') and request.POST.get('CID') and request.POST.get('village') and request.POST.get('email'):
			savest=attendance()#class name of model 
			savest.name=request.POST.get('name')
			savest.phonenumber=request.POST.get('phonenumber') 
			savest.CID=request.POST.get('CID')
			savest.village=request.POST.get('village')
			savest.email=request.POST.get('email')
			savest.save()
			messages.success(request,"The Record"+""+""+savest.name+ "" +"Is Saved Successfully...")
		return render(request,'Create.html')
	else:
		return render(request,'Create.html') 	

def edit(request,id):# to get id 
	getstudentdetails=attendance.objects.get(id=id)
	return render(request,'edit.html',{"attendance":getstudentdetails})#adding dict first is key and second is value			

def updatest(request,id):
	updatest=attendance.objects.get(id=id)
	form=stforms(request.POST,instance=updatest)#it will take two parameter
	if form.is_valid():#condition
		form.save()#it will save the date 
		messages.success(request,"The Student Record is Update successfully..!")
		return render(request,'edit.html',{"attendance":updatest})

def deletest(request,id):
    delstudent=attendance.objects.get(id=id)
    delstudent.delete()
    results=attendance.objects.all()
    return render(request,'atten.html',{"attendance":deletest}) 


def insert(request):
	if request.method=="POST":#condition
		#value that we gonna post
		if request.POST.get('Time')  and request.POST.get('Date') and request.POST.get('Course') and request.POST.get('Nima') and request.POST.get('Norbu_Dorji') and request.POST.get('Pema_Chophel') and request.POST.get('Pema_Lhamo') and request.POST.get('Pema_Norbu'):
			savest=record()#class name of model 
			savest.Time=request.POST.get('Time')
			savest.Date=request.POST.get('Date')
			savest.Course=request.POST.get('Course')
			savest.Nima=request.POST.get('Nima')
			savest.Norbu_Dorji=request.POST.get('Norbu_Dorji') 
			savest.Pema_Chophel=request.POST.get('Pema_Chophel')
			savest.Pema_Lhamo=request.POST.get('Pema_Lhamo')
			savest.Pema_Norbu=request.POST.get('Pema_Norbu')
			savest.save()
			messages.success(request,"The Record" +""+"Is Save Successfully...")
		return render(request,'record.html')
	else:
		return render(request,'record.html')


def about(request):
	return render(request,'about.html')	

def timetable(request):
	return render(request,'Timetable.html')

def time(request):
	return render(request,'time.html')





# def setcookies(request):
#     response=render(request, 'Student/setcookies.html')
#     response.set_cookies('name','pemanorbu')
#     return response		


           


















		


