from django.contrib import admin
from django.urls import path,include
from .import views


urlpatterns=[
	path('admin/',admin.site.urls),
	path('',views.signnewuser, name="signnewuser"),
	path('login',views.loginuser,name="loginuser"),
	path('home',views.home,name="home"),
	path('logout',views.logoutpage,name="logoutpage"),
	path('stu/', views.studentinfo,name="studentinfo"),#path to display student attendance
	path('',views.stdisplay, name="stdisplay"),
    path('Create',views.stinsert ,name="stinsert"),
    path('edit/<int:id>',views.edit, name="edit"),#
    path('update/<int:id>',views.updatest),
    path('delete/<int:id>',views.deletest),
    path('record',views.insert ,name="insert"),
    path('about',views.about, name='about'),
    path('timetable',views.timetable, name='timetable'),
	path('time',views.time, name='time'),

    # path('setcookies',views.setcookies),
]