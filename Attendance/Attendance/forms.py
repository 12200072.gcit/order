from django import forms
from Attendance.models import attendance

class stforms(forms.ModelForm):
	class Meta:#is just a class container with some options (metadata) attached to the model.
		model=attendance
		fields="__all__"